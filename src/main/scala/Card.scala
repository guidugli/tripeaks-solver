
class Card(val c: String, val s: String) {

  val card = c.toUpperCase match {
    case "A" => "A"
    case "2" => "2"
    case "3" => "3"
    case "4" => "4"
    case "5" => "5"
    case "6" => "6"
    case "7" => "7"
    case "8" => "8"
    case "9" => "9"
    case "10" => "10"
    case "J" | "JACK" => "J"
    case "Q" | "QUEEN" => "Q"
    case "K" | "KING" => "K"
    case _ => println("Invalid card specified.") ; ""

  }

  val suit = s.toUpperCase match {
    case "HEARTS" => "Hearts"
    case "SPADES" => "Spades"
    case "CLUBS" => "Clubs"
    case "DIAMONDS" => "Diamonds"
    case _ => println("Invalid suit specified.") ; ""
  }

  def getNextCardValue(): String = {
    card match {
      case "A" => "2"
      case "2" => "3"
      case "3" => "4"
      case "4" => "5"
      case "5" => "6"
      case "6" => "7"
      case "7" => "8"
      case "8" => "9"
      case "9" => "10"
      case "10" => "J"
      case "J" | "JACK" => "Q"
      case "Q" | "QUEEN" => "K"
      case "K" | "KING" => "A"
    }
  }

  def getPreviousCardValue(): String = {
    card match {
      case "A" => "K"
      case "2" => "1"
      case "3" => "2"
      case "4" => "3"
      case "5" => "4"
      case "6" => "5"
      case "7" => "6"
      case "8" => "7"
      case "9" => "8"
      case "10" => "9"
      case "J" | "JACK" => "10"
      case "Q" | "QUEEN" => "J"
      case "K" | "KING" => "Q"
    }
  }

}
