class Tree[A](val value: A) {

  var children = List[Tree[A]]()

  def addChild(tree: Tree[A]): Unit ={
    children ::= tree
  }

  def getNodes():List[List[A]] = {

    if (children.isEmpty){
      List(List(value))
    } else {
      var result = List[List[A]]()
      for (c <- children){
        val cr = c.getNodes()
        for (l <- cr) {
          result ::= List(value) ::: l
        }
      }
      result
    }

  }

}
