/*
           25                26                27
        19    20          21    22          23    24
     10    11    12    13    14    15    16    17    18
  0     1     2     3     4     5     6     7     8     9
 */



import scala.collection.mutable.ListBuffer
object TriPeaks {

  val deck: Array[Card] = Array(
    new Card("6", "Diamonds"),
    new Card("A", "Diamonds"),
    new Card("5", "Clubs"),
    new Card("9", "Hearts"),
    new Card("5", "Spades"),
    new Card("4", "Clubs"),
    new Card("Q", "Clubs"),
    new Card("3", "Hearts"),
    new Card("Q", "Hearts"),
    new Card("6", "Clubs"),
    new Card("4", "Diamonds"),
    new Card("8", "Diamonds"),
    new Card("7", "Diamonds"),
    new Card("10", "Spades"),
    new Card("9", "Diamonds"),
    new Card("K", "Clubs"),
    new Card("7", "Hearts"),
    new Card("7", "Clubs"),
    new Card("10", "Diamonds"),
    new Card("6", "Spades"),
    new Card("2", "Clubs"),
    new Card("4", "Spades"),
    new Card("8", "Hearts"),
    new Card("2", "Spades")
  )

  val tree: Array[Option[Card]] = Array(
    Some(new Card("8", "Spades")),
    Some(new Card("A", "Hearts")),
    Some(new Card("3", "Spades")),
    Some(new Card("2", "Hearts")),
    Some(new Card("K", "Hearts")),
    Some(new Card("10", "Clubs")),
    Some(new Card("A", "Clubs")),
    Some(new Card("5", "Hearts")),
    Some(new Card("Q", "Diamonds")),
    Some(new Card("A", "Spades")),
    //
    Some(new Card("3", "Diamonds")),
    Some(new Card("J", "Clubs")),
    Some(new Card("8", "Clubs")),
    Some(new Card("J", "Hearts")),
    Some(new Card("Q", "Spades")),
    Some(new Card("9", "Spades")),
    Some(new Card("9", "Clubs")),
    Some(new Card("10", "Hearts")),
    Some(new Card("7", "Spades")),
    //
    Some(new Card("4", "Hearts")),
    Some(new Card("J", "Spades")),
    Some(new Card("5", "Diamonds")),
    Some(new Card("6", "Hearts")),
    Some(new Card("K", "Spades")),
    Some(new Card("J", "Diamonds")),
    //
    Some(new Card("3", "Clubs")),
    Some(new Card("K", "Diamonds")),
    Some(new Card("2", "Diamonds"))
  )

  def main(args: Array[String]): Unit = {

    var t = solve(deck(0), 0, tree)
    if (t._2) {
      var r = t._1.getNodes()

      for (l <- r) {
        if (l.forall(_.isDefined)) {
          l.foreach(m => print(m.get.card + " of "  +  m.get.suit +", "))
          println()
        }
      }
    }
  }

  def solve(base: Card, deckpos: Int, tree: Array[Option[Card]]): (Tree[Option[Card]], Boolean) ={
    var z=0
    tree.foreach( y => if (y.isEmpty) z +=1)
    //println("Card: " + base.card + ", Length : " + tree.length + ", Nones: " + z + ", deckpos: " + deckpos + ", decksize: " + deck.length)
    val matches = getMatches(base.card, tree)

    val result = new Tree(Option(base))
    var solved = false;


    if (tree.forall(_.isEmpty)) {
      //println("WIN")
      solved = true;
      // nothing to do - win condition
    } else if ((deckpos+1 >= deck.length) && matches.isEmpty) {
      // No more moves available
      //println("LOOSE")
      result.addChild(new Tree(None))
    } else {

      for (m <- matches) {
        if (!solved) {
          //println("Base: " + base.card + ", from MATCH " + m + "[" + tree(m).get.card + "]")
          var newtree = tree.clone()
          newtree(m) = None
          val r = solve(tree(m).get, deckpos, newtree)
          solved = r._2
          if (solved) result.addChild(r._1)

        }
      }

      if (deckpos+1 < deck.length) {
        if (!solved) {
          //println("from deck, deckpos: " + (deckpos + 1) + ", base: " + deck(deckpos + 1).card)
          val r = solve(deck(deckpos + 1), deckpos + 1, tree)
          solved = r._2
          if (solved) result.addChild(r._1)

        }
      }
    }
    //println("End of solve... Card: " + base.card + ", Length : " + tree.length + ", deckpos: " + deckpos + ", decksize: " + deck.length)
    (result, solved)

  }

  def getMatches(card: String, t: Array[Option[Card]]):List[Int] = {
    var i = 0
    var result = new ListBuffer[Int]
    for (i <- 0 to t.length){
      if ((i < 10) && (t(i).isDefined) && ((t(i).get.getPreviousCardValue() == card) || (t(i).get.getNextCardValue() == card))){
        result += i
      } else if ((i >= 10) && (i <= 18) && (t(i).isDefined) && (t(i-9).isEmpty) && (t(i-10).isEmpty) && ((t(i).get.getPreviousCardValue() == card) || (t(i).get.getNextCardValue() == card))){
        result += i
      } else {
        i match {
          case 19 => if (t(19).isDefined && t(10).isEmpty && t(11).isEmpty && ((t(19).get.getPreviousCardValue() == card) || (t(19).get.getNextCardValue() == card))) { result += i}
          case 20 => if (t(20).isDefined && t(11).isEmpty && t(12).isEmpty && ((t(20).get.getPreviousCardValue() == card) || (t(20).get.getNextCardValue() == card))) { result += i}
          case 21 => if (t(21).isDefined && t(13).isEmpty && t(14).isEmpty && ((t(21).get.getPreviousCardValue() == card) || (t(21).get.getNextCardValue() == card))) { result += i}
          case 22 => if (t(22).isDefined && t(14).isEmpty && t(15).isEmpty && ((t(22).get.getPreviousCardValue() == card) || (t(22).get.getNextCardValue() == card))) { result += i}
          case 23 => if (t(23).isDefined && t(16).isEmpty && t(17).isEmpty && ((t(23).get.getPreviousCardValue() == card) || (t(23).get.getNextCardValue() == card))) { result += i}
          case 24 => if (t(24).isDefined && t(17).isEmpty && t(18).isEmpty && ((t(24).get.getPreviousCardValue() == card) || (t(24).get.getNextCardValue() == card))) { result += i}
          case 25 => if (t(25).isDefined && t(19).isEmpty && t(20).isEmpty && ((t(25).get.getPreviousCardValue() == card) || (t(25).get.getNextCardValue() == card))) { result += i}
          case 26 => if (t(26).isDefined && t(21).isEmpty && t(22).isEmpty && ((t(26).get.getPreviousCardValue() == card) || (t(26).get.getNextCardValue() == card))) { result += i}
          case 27 => if (t(27).isDefined && t(23).isEmpty && t(24).isEmpty && ((t(27).get.getPreviousCardValue() == card) || (t(27).get.getNextCardValue() == card))) { result += i}
          case _ => 0 // do nothing
        }
      }
    }
    result.toList
  }

}
